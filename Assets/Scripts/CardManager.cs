using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CardManager : MonoBehaviour
{
    public static CardManager instance;

    // List of Card Data
    [SerializeField]
    public List<CardClass> cardData;

    [SerializeField]
    private GameObject cardHolder;

    public Sprite cardOpenImg;

    private int cardSelected;

    // Random Card for player who just connect
    public int selectRandomCard()
    {
        cardSelected = Random.Range(1, (cardData.Count - 1));
        return cardSelected;
    }
}
