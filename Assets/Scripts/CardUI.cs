using UnityEngine;
using UnityEngine.UI;

namespace Mirror.Examples.Basic
{
    public class CardUI : NetworkBehaviour
    {
        [Header("Player Components")]
        public Image image;

        [Header("Child Text Objects")]
        public Text CardIDText;
        public Text CardNameText;
        public Text CardDescText;

        public bool selected;

        Card card;
        CardManager CardManager;

        /// <summary>
        /// Caches the controlling Player object, subscribes to its events
        /// </summary>
        /// <param name="player">Player object that controls this UI</param>
        /// <param name="isLocalPlayer">true if the Player object is the Local Player</param>
        public void SetPlayerCard(Card card, bool isLocalPlayer)
        {
            // cache reference to the player that controls this UI object
            this.card = card;

            CardManager = FindObjectOfType<CardManager>();
            int selectedCard = CardManager.selectRandomCard();

            CardIDText.text = CardManager.cardData[selectedCard].CardID;
            CardNameText.text = CardManager.cardData[selectedCard].CardName;
            CardDescText.text = CardManager.cardData[selectedCard].CardDesc;

            CardIDText.color = new Color(1f, 1f, 1f, 0f);
            CardNameText.color = new Color(1f, 1f, 1f, 0f);
            CardDescText.color = new Color(1f, 1f, 1f, 0f);

        }

        void OnDisable()
        {
            card.OnCardIDChanged -= OnCardIDChanged;
            card.OnCardNameChanged -= OnCardNameChanged;
            card.OnCardDescChanged -= OnCardDescChanged;
        }

        // This value can change as clients leave and join
        void OnCardIDChanged(string newCardID)
        {
            CardNameText.text = newCardID;
        }

        void OnCardNameChanged(string newCardName)
        {
            CardNameText.text = newCardName;
        }

        void OnCardDescChanged(string newCardDesc)
        {
            // Show the data in the UI
            CardDescText.text = newCardDesc;
        }

        public void SelectedCard()
        {
            // get our player
            Card card = NetworkClient.connection.identity.GetComponent<Card>();

            card.CardSend(this.transform.GetChild(0).GetComponent<Text>().text, 
                this.transform.GetChild(1).GetComponent<Text>().text, 
                this.transform.GetChild(2).GetComponent<Text>().text);
        }
    }
}
