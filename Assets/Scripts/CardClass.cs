using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class CardClass
{
    //Class for Card
    public string CardID;
    public string CardName;
    public string CardDesc;
    public Sprite FrontCard;
    public Sprite BackCard;
}
