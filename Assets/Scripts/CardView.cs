using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CardView : MonoBehaviour
{
    private Image imgCard;
    private GameObject OpenPanel;
    private Text IDCard;
    private Text NameCard;
    private Text DescCard;
    
    // Set the Card Image from cardSprite variable.
    public void SetCard(CardClass cardData)
    {
        imgCard.sprite = cardData.BackCard;
        IDCard.text = cardData.CardID;
        NameCard.text = cardData.CardName;
        DescCard.text = cardData.CardDesc;
        OpenPanel.SetActive(false);
    }


}
