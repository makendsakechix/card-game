using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Mirror.Examples.Basic
{
    public class Card : NetworkBehaviour
    {
        // Events that the UI will subscribe to
        public event System.Action<string> OnCardIDChanged;
        public event System.Action<string> OnCardNameChanged;
        public event System.Action<string> OnCardDescChanged;

        // Players List to manage playerNumber
        static readonly List<Card> playersList = new List<Card>();

        internal static void ResetCardIDs()
        {
            int cardID = 0;
            foreach (Card player in playersList)
                player.cardID = ""+cardID++;
        }

        [Header("Card UI")]
        public GameObject cardUIPrefab;
        GameObject cardUI;

        [Header("Selected Card SyncVars")]

        /// <summary>
        /// This is appended to the player name text, e.g. "Player 01"
        /// </summary>
        [SyncVar(hook = nameof(CardIDChanged))]
        public string cardID = "";

        /// <summary>
        /// This is updated by UpdateData which is called from OnStartServer via InvokeRepeating
        /// </summary>
        [SyncVar(hook = nameof(CardNameChanged))]
        public string cardName = "";

        /// <summary>
        /// Random color for the playerData text, assigned in OnStartServer
        /// </summary>
        [SyncVar(hook = nameof(CardDescChanged))]
        public string cardDesc = "";

        // This is called by the hook of cardId SyncVar above
        void CardIDChanged(string _, string newCardID)
        {
            OnCardIDChanged?.Invoke(newCardID);
        }

        // This is called by the hook of cardName SyncVar above
        void CardNameChanged(string _, string newCardName)
        {
            OnCardNameChanged?.Invoke(newCardName);
        }

        // This is called by the hook of cardDecs SyncVar above
        void CardDescChanged(string _, string newCardDesc)
        {
            OnCardDescChanged?.Invoke(newCardDesc);
        }

        [Command]
        public void CardSend(string id, string name,string desc)
        {
            Debug.Log("Recive from Client"+desc);
            cardID = id;
            cardName = name;
            cardDesc = desc;
            RpcReceive(id, name, desc);
        }

        [ClientRpc]
        public void RpcReceive(string id, string name, string desc)
        {
            OnCardIDChanged?.Invoke(id);
            OnCardNameChanged?.Invoke(id);
            OnCardDescChanged?.Invoke(id);
        }


        /// <summary>
        /// This is invoked for NetworkBehaviour objects when they become active on the server.
        /// <para>This could be triggered by NetworkServer.Listen() for objects in the scene, or by NetworkServer.Spawn() for objects that are dynamically created.</para>
        /// <para>This will be called for objects on a "host" as well as for object on a dedicated server.</para>
        /// </summary>
        public override void OnStartServer()
        {
            base.OnStartServer();

            // Add this to the static Players List
            playersList.Add(this);
            
        }

        /// <summary>
        /// Invoked on the server when the object is unspawned
        /// <para>Useful for saving object data in persistent storage</para>
        /// </summary>
        public override void OnStopServer()
        {
            CancelInvoke();
            playersList.Remove(this);
        }

        [ClientCallback]

        private void Awake()
        {
            // Start generating updates
            InvokeRepeating(nameof(UpdateData), 1, 1);
        }

        // This only runs on the client
        [ClientCallback]
        void UpdateData()
        {
            // Check Card Holder Child if there same card id with the selected then open it up.
            Transform parentCardUI = cardUI.transform.parent;
            Debug.Log(parentCardUI.name);

            foreach (Transform child in parentCardUI)
            {
                if (cardID == child.transform.GetChild(0).GetComponent<Text>().text)
                {
                    CardManager CardM = parentCardUI.GetComponent<CardManager>();
                    child.GetComponent<Image>().sprite = CardM.cardOpenImg;
                    child.transform.GetChild(0).GetComponent<Text>().color = new Color(1f, 1f, 1f, 1f);
                    child.transform.GetChild(1).GetComponent<Text>().color = new Color(1f, 1f, 1f, 1f);
                    child.transform.GetChild(2).GetComponent<Text>().color = new Color(1f, 1f, 1f, 1f);
                }
            }
        }

        /// <summary>
        /// Called on every NetworkBehaviour when it is activated on a client.
        /// <para>Objects on the host have this function called, as there is a local client on the host. The values of SyncVars on object are guaranteed to be initialized correctly with the latest state from the server when this function is called on the client.</para>
        /// </summary>
        public override void OnStartClient()
        {
            // Activate the main panel
            ((CardNetManager)NetworkManager.singleton).mainPanel.gameObject.SetActive(true);

            

            for (int i = 0; i < 3; i++)
            {
                // Instantiate the player UI as child of the Players Panel
                cardUI = Instantiate(cardUIPrefab, ((CardNetManager)NetworkManager.singleton).cardsPanel);

                // Set this player object in PlayerUI to wire up event handlers
                cardUI.GetComponent<CardUI>().SetPlayerCard(this, isLocalPlayer);

            }

            

            
        }

        /// <summary>
        /// This is invoked on clients when the server has caused this object to be destroyed.
        /// <para>This can be used as a hook to invoke effects or do client specific cleanup.</para>
        /// </summary>
        public override void OnStopClient()
        {
            // Remove entery card player's UI object
            Transform parentCardUI = cardUI.transform.parent;
            foreach (Transform child in parentCardUI)
            {
                GameObject.Destroy(child.gameObject);
            }

            // Disable the main panel for local player
            if (isLocalPlayer)
                ((CardNetManager)NetworkManager.singleton).mainPanel.gameObject.SetActive(false);
        }
    }
}